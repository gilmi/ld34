01
--

A game made in 72 hours during LD34 with [PureScript](http://purescript.org) (-canvas, -signal, etc).

- [Play](https://soupi.github.io/ld34)
- [View on LudumDare site](http://ludumdare.com/compo/ludum-dare-34/?action=preview&uid=29243)


